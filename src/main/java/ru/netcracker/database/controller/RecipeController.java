package ru.netcracker.database.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.netcracker.database.entity.Recipe;
import ru.netcracker.database.service.RecipeService;

import java.util.List;

@RestController
@RequestMapping("/recipe")
public class RecipeController {

    @Autowired
    private RecipeService recipeService;

    @GetMapping()
    public Recipe getRecipeById(@RequestParam Long id){
        return recipeService.getRecipeById(id);
    }

    @GetMapping("/all")
    public List<Recipe> getAllRecipes(){
        return recipeService.getAllRecipes();
    }

    @PostMapping("/add")
    public Recipe addRecipe(@RequestParam String name,
                            @RequestParam String description){
        return recipeService.addRecipe(name, description);
    }

    @GetMapping("/name")
    public List<Recipe> getRecipeByName(@RequestParam String name){
        return recipeService.getRecipeByName(name);
    }
    @GetMapping("/description")
    public List<Recipe> getRecipeBuDescription(@RequestParam String description){
        return recipeService.getRecipeByDescription(description);
    }
}
