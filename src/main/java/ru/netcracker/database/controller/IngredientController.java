package ru.netcracker.database.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.netcracker.database.entity.Ingredient;
import ru.netcracker.database.service.IngredientService;

import java.util.List;

@RestController
@RequestMapping("/ingredients")
public class IngredientController {

    @Autowired
    public IngredientService ingredientService;

    @PostMapping("/add")
    public Ingredient addIngredient(@RequestParam String name,
                                    @RequestParam int amount){
        return ingredientService.addIngredient(name, amount);
    }

    @GetMapping()
    public Ingredient getIngredientById(@RequestParam Long id){
        return ingredientService.getIngredientById(id);
    }

    @GetMapping("/amount")
    public List<Ingredient> getIngredientByAmount(@RequestParam int amount){
        return ingredientService.getIngredientByAmount(amount);
    }

}
