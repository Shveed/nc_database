package ru.netcracker.database.repository;

import ru.netcracker.database.entity.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {

    List<Recipe> getRecipeByName(String name);

    List<Recipe> getRecipeByDescription(String description);
}
