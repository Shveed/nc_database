package ru.netcracker.database.repository;

import ru.netcracker.database.entity.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, Long> {

    List<Ingredient> getIngredientByAmount(int amount);

    List<Ingredient> getIngredientByName(String name);
}
