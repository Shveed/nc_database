package ru.netcracker.database.entity;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "recipe")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String description;

    @OneToMany(mappedBy = "recipe")
    @ManyToMany(mappedBy = "recipes")
    private List<Ingredient> ingredientsIds;

    public Recipe(String name, String description){
        this.name = name;
        this.description = description;
    }
}
