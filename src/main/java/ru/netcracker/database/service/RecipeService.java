package ru.netcracker.database.service;

import ru.netcracker.database.entity.Recipe;

import java.util.List;

public interface RecipeService {

    Recipe addRecipe(String name, String description);

    Recipe getRecipeById(Long id);

    List<Recipe> getRecipeByName(String name);

    List<Recipe> getRecipeByDescription(String description);

    List<Recipe> getAllRecipes();
}
