package ru.netcracker.database.service.impl;

import ru.netcracker.database.entity.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.netcracker.database.repository.IngredientRepository;
import ru.netcracker.database.service.IngredientService;

import java.util.List;

@Service
public class IngredientServiceImpl implements IngredientService {

    @Autowired
    private IngredientRepository ingredientRepository;

    @Autowired
    public Ingredient addIngredient(String name, int amount){
        Ingredient ingredient = new Ingredient(name, amount);
        ingredientRepository.save(ingredient);
        return ingredient;
    }

    public Ingredient getIngredientById(Long id){
        return ingredientRepository.findById(id).orElse(null);
    }

    public List<Ingredient> getAllIngredients(){
        return ingredientRepository.findAll();
    }

    public List<Ingredient> getIngredientByAmount(int amount){
        return ingredientRepository.getIngredientByAmount(amount);
    }

    public List<Ingredient> getIngredientByName(String name){
        return ingredientRepository.getIngredientByName(name);
    }
}
