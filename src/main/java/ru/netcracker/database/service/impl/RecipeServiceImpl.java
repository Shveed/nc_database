package ru.netcracker.database.service.impl;

import ru.netcracker.database.entity.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.netcracker.database.repository.RecipeRepository;
import ru.netcracker.database.service.IngredientService;
import ru.netcracker.database.service.RecipeService;

import java.util.List;

@Service
public class RecipeServiceImpl implements RecipeService {

    @Autowired
    private IngredientService ingredientService;

    @Autowired
    private RecipeRepository recipeRepository;

    public Recipe addRecipe(String name, String description){
        Recipe recipe = new Recipe(name, description);
        recipeRepository.save(recipe);
        return recipe;
    }

    public Recipe getRecipeById(Long id){
        return recipeRepository.findById(id).orElse(null);
    }

    public List<Recipe> getRecipeByName(String name){
        return recipeRepository.getRecipeByName(name);
    }

    public List<Recipe> getRecipeByDescription(String description){
        return recipeRepository.getRecipeByDescription(description);
    }

    public List<Recipe> getAllRecipes(){
        return recipeRepository.findAll();
    }
}
