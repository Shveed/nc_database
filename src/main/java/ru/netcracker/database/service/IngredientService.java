package ru.netcracker.database.service;

import ru.netcracker.database.entity.Ingredient;

import java.util.List;

public interface IngredientService {

    Ingredient addIngredient(String name, int amount);

    Ingredient getIngredientById(Long id);

    List<Ingredient> getAllIngredients();

    List<Ingredient> getIngredientByAmount(int amount);

    List<Ingredient> getIngredientByName(String name);
}
